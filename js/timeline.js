
window.onload = function() {
	var timeline_source = timeline_source_json.sort(sort_by('start_date', false));
	
	draw_timeline(timeline_source);
};

// https://github.com/mquan/timeline/blob/master/js/timeline.js
// http://www.tizag.com/javascriptT/javascriptdate.php
// http://stoicloofah.github.com/chronoline.js/

// draw the timeline
draw_timeline = function (source) {
	
	
	// Define some parameters
	// width and height of the timeline (= paper)
	var paper_width = 900;
	var paper_height = 240;
	
	var timeline_padding = 200;
	var timeline_width = paper_width - timeline_padding;
	var timeline_height = paper_height - 100;
	
	// Current day
	var current_date = new Date();
	
	// Get the earliest start date of the set of date arrays.
	var min_start_date = getMinStartDate(source);
	
	// The last date to show on the timeline is last day of this year.
	var max_date = new Date(current_date.getFullYear(),11,31);
	console.log('max_date: ' + max_date);
	
	// The first date to show on the timeline is first day of the year from the min_start_date.
	var min_date = new Date(min_start_date.getFullYear(),0,1);
	console.log('min_date: ' + min_date);
	
	// Timespan between first day of the first year and last day of the last year
	// We want to show whole years
	
	// in days
	var number_of_days = Date.dateDiff('d', min_date, max_date);
	console.log('number_of_days: ' + number_of_days);
	// in years
	var number_of_years = Date.dateDiff('y', min_date, max_date) + 1;
	console.log('number_of_years: ' + number_of_years);
	
	// Define the length in px per day or year
	var px_per_day = timeline_width / number_of_days;
	var px_per_year = timeline_width / number_of_years;

	// Create the canvas container for Raphael
	var paper = new Raphael(document.getElementById('canvas_container'), paper_width, paper_height);
	// Create a set to hold all elements
	var timeline_set = paper.set();
	var timeline_box_set = paper.set();
	var timeline_label_set = paper.set();
	
	// Draw the timeline
	// ******************
	// Baseline
	timeline_set.push(
		paper.path('M ' + (timeline_padding / 2) + ' ' + (timeline_height) + ' l ' + timeline_width + ' 0 z')
	);

	// Year notches and labels
	for(var i = 0; i < (number_of_years); i++){
		// Draw little notch
		timeline_set.push(
			paper.path('M ' + ((timeline_padding / 2) + (i * px_per_year)) + ' ' + (timeline_height) + ' l 0 10 z')
		);
		// Put the year label
		timeline_set.push(
			paper.text(((timeline_padding / 2) + (i * px_per_year) + (px_per_year / 2)), (timeline_height + 20), (min_date.getFullYear() + i))
		);
	}
	
	// Today notch
    // Get the coordinates of the current date
    // Calculate the difference between min_date and current_date in days
    var current_date_diff_days = Date.dateDiff('d', min_date, current_date);
    // Convert the difference in pixels
    var current_date_diff_pixels = current_date_diff_days * px_per_day;
    timeline_set.push(
    	today = paper.circle((current_date_diff_pixels + (timeline_padding / 2)), timeline_height, 5)
    );
    
	today.attr({
		fill: '#444444',
		stroke: 'none',
	});
	
	// Blocks
	// ******************
	
	// Initiate array to capture the end dates of the set
    var end_dates_collection = [];
    // Keep an array of all used types, to be combined with color schemes.
    var types = [];
    
	for (var i = 0; i < source.length; ++i) {
		
	    console.log(source[i]);
	    
	    // Check if the type exists already
	    if (!(contains(types, source[i].type))) {
	    	// Push it to the array so we can find it next time
	    	types.push(source[i].type);
	    	// Deifne height
	    	var height = timeline_height + 40 + types.length * 12;
	    	// Draw an indicative block with the correct color
	    	type = paper.rect((timeline_padding / 2), height, 10, 10);
	    	type.attr({
	    		fill: box_color[Math.min(types.length - 1, box_color.length - 1)][0],
	    		stroke: 'none',
	    	});
	    	// Draw the text fitting for the box
	    	paper.text((timeline_padding / 2) + 30, height + 5, source[i].type);
	    }
	    // Just in case not enough colors were defined, take the last color instead of throwing an error
	    color_index = Math.min(types.length - 1, box_color.length - 1);
	    
	    // Define start and end date.
	    // Dates can not be in the future.
	    var start_date = new Date(Math.min(new Date(source[i].start_date), current_date));
	    var end_date = new Date(Math.min(new Date(source[i].end_date), current_date));
	    
	    // Get the coordinates of the start date
	    // Calculate the difference between min_date and start_date in days
	    var start_date_diff_days = Date.dateDiff('d', min_date, start_date);
	    // Convert the difference in pixels
	    var start_date_diff_pixels = start_date_diff_days * px_per_day;
	    
	    // Get the coordinates of the end date
	    var end_date_diff_days = Date.dateDiff('d', min_date, end_date);
	    // Convert the difference in pixels
	    var end_date_diff_pixels = end_date_diff_days * px_per_day;
	    
	    // Height of the box
	    
	    // Default layer is 0
	    var layer = 0;
	    // Loop through all the previous end dates with their layers
	    for (var dates_length = 0; dates_length < end_dates_collection.length; ++dates_length){
	    	// Check if the current start date overlaps with an end date
	    	if (start_date < end_dates_collection[dates_length][0]) {
	    		// If there is an overlap, check if the overlapping end date's layer.
	    		if (end_dates_collection[dates_length][1] >= layer) {
	    			layer = end_dates_collection[dates_length][1] + 1;
	    		}    		
	    	}
	    }
	    
	    // Push the end date to the collection, needed to detect overlaps
	    end_dates_collection.push([end_date, layer]);
	    // Draw the box higher if there is overlap
	    var height = 50 + (layer * 10);
	    
	    // Draw the box
	    var box = paper.rect(((timeline_padding / 2) + start_date_diff_pixels), (timeline_height - height), Math.max((end_date_diff_pixels - start_date_diff_pixels),4), height);
	    
	    box.attr({  
            fill: box_color[color_index][i % box_color[color_index].length],
            stroke: 'none',
            opacity: 0.6, 
		});
	    
	    // Define the label
	    var label = paper.text((box.getBBox().x + (box.getBBox().width / 2)), 50, cutLine(source[i].title,30));
	    label.attr({
	    	fill: text_color,
	    });
	    
	    // Animate the box when hovering over the box or label
	    box.hover(
	    	function(){
				this.animate({
					opacity: 1, 
				}, 200);
				this.g = this.glow({color: "#fff", width: 8});
	    	}, 
		    function(){
	            this.animate({
	                opacity: 0.6, 
	            }, 200);
	            this.g.remove();
	       }, box, box);

	    label.hover(
	    	function(){
				this.animate({
					opacity: 1, 
				}, 200);
				this.g = this.glow({color: "#fff", width: 8});
		    },
		    function(){
		    	this.animate({
	                opacity: 0.6, 
	            }, 200);
		    	this.g.remove();
		    }, box, box);
	    
//	     https://groups.google.com/forum/?fromgroups=#!topic/raphaeljs/6QVxUZWPWTk
    
	    // Make sure the labels are within the timeline box
	    if (label.getBBox().x < 0) {
	    	label.transform("...t" + (- label.getBBox().x) + ",0");
	    }
	    if (label.getBBox().x2 > paper_width) {
	    	label.transform("...t" + (paper_width - label.getBBox().x2) + ",0");
	    }
	    
	    // Check to see if there is any overlap with previous labels
//	    http://raphaeljs.com/reference.html#Set.forEach
	    timeline_label_set.forEach(function (el) {
	    // http://raphaeljs.com/reference.html#Raphael.isBBoxIntersect
	    	if (Raphael.isBBoxIntersect(label.getBBox(), el.getBBox())) {
	    		// http://raphaeljs.com/reference.html#Element.transform
	    		label.transform("...t0,-15");
	    		
	    	}
	    });
	   
	    // Push this box and label to the set
	    timeline_box_set.push(box);
	    timeline_label_set.push(label);
	    
	    var pair = $(label.node).add($(box.node));
	    
	 // Create the tooltip
	    var tip_config = {
			content: {
				text:  '<em>' + source[i].start_date + ' to ' + source[i].end_date + '</em><br/>' + source[i].summary,
				title: {
					text: source[i].title
				}
			},
			style: {
				classes: 'ui-tooltip-rounded qtip-dark'
			},
//			http://craigsworks.com/projects/forums/thread-solved-getting-tooltip-to-appear-on-different-element-with-the-same-class-name
			show: { target: pair },
			hide: { target: pair },
//			http://craigsworks.com/projects/qtip2/docs/position/
			position: {
				viewport: $(window),
				my: 'top center',
			    at: 'bottom center',
			},
	    };   
	    $(box.node).qtip(tip_config);
	}
	// Make sure the whole timeline is on front
	timeline_set.toFront();
};


//http://www.htmlgoodies.com/html5/javascript/calculating-the-difference-between-two-dates-in-javascript.html#fbid=q1bH1aPaooO

//datepart: 'y', 'm', 'w', 'd', 'h', 'n', 's'
Date.dateDiff = function(datepart, fromdate, todate) {	
datepart = datepart.toLowerCase();	
var diff = todate - fromdate;	
var divideBy = { y:31557600000,
		  		   m:2635200000,
		  		   w:604800000, 
                d:86400000, 
                h:3600000, 
                n:60000, 
                s:1000 };	

return Math.floor( diff/divideBy[datepart]);
};

//Find the earliest data in a set of dates.
function getMinStartDate(items) {
 if (items && items.length > 0) {
     var current_date = new Date();
     var min_date = current_date;
     $.each(items, function (i, item) {
         var start_date = new Date(item.start_date);
         if (start_date < min_date) {
             min_date = start_date;
         }
     });
     return min_date;
 }
 return null;
};    

function cutLine(st, n) {
	// Take three more, otherwise it does not make sense to replace the characters with three dots, which are in the end also characters
	if (st.length > (n + 3)) {
		return st.substr(0,n) + '...';
	}
	else {
		return st;
	}
	
}

function contains(a, obj) {
    for (var i = 0; i < a.length; i++) {
        if (a[i] === obj) {
            return true;
        }
    }
    return false;
}

// Function to sort on start date
// http://stackoverflow.com/questions/979256/how-to-sort-an-array-of-javascript-objects
var sort_by = function(field, reverse, primer){
   var key = function (x) {return primer ? primer(x[field]) : x[field]};

   return function (a,b) {
       var A = key(a), B = key(b);
       return (A < B ? -1 : (A > B ? 1 : 0)) * [1,-1][+!!reverse];
   }
}